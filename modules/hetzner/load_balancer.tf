resource "hcloud_load_balancer" "load_balancer" {
  for_each = var.load_balancer_spec

  name               = each.key
  load_balancer_type = each.value.load_balancer_type
  network_zone       = each.value.network_zone
  labels             = each.value.labels
  location           = each.value.location
  delete_protection  = each.value.delete_protection

  algorithm {
    type = each.value.algorithm.type
  }
}

resource "hcloud_load_balancer_network" "load_balancer_network" {
  for_each = var.load_balancer_spec

  load_balancer_id = hcloud_load_balancer.load_balancer[each.key].id
  network_id       = hcloud_network.network[each.value.network.name].id
  ip               = each.value.network.ip

  depends_on = [
    hcloud_network.network,
    hcloud_network_subnet.network_subnet,
  ]
}

resource "hcloud_load_balancer_target" "load_balancer_target" {
  for_each = {
    for obj in flatten([
      for lb_name, lb_spec in var.load_balancer_spec : [
        for target_name, target_spec in lb_spec.targets :
        merge({
          _lb_name     = lb_name,
          _target_name = target_name
        }, target_spec)
      ]
    ]) : "${obj._lb_name}|${obj._target_name}" => obj
  }

  type             = "label_selector"
  load_balancer_id = hcloud_load_balancer.load_balancer[each.value._lb_name].id
  label_selector   = each.value.label_selector
  use_private_ip   = each.value.use_private_ip

  depends_on = [
    hcloud_network.network,
    hcloud_network_subnet.network_subnet,
    hcloud_load_balancer_network.load_balancer_network
  ]
}

resource "hcloud_load_balancer_service" "load_balancer_service" {
  for_each = {
    for obj in flatten([
      for lb_name, lb_spec in var.load_balancer_spec : concat(
        [
          for service_name, service_spec in lb_spec.services_http :
          merge({
            _lb_name      = lb_name,
            _service_name = service_name
            protocol      = service_spec.protocol # http or https
          }, service_spec)
        ],
        [
          for service_name, service_spec in lb_spec.services_tcp :
          merge({
            _lb_name      = lb_name,
            _service_name = service_name,
            protocol      = "tcp"
          }, service_spec)
        ]
      )
    ]) : "${obj._lb_name}|${obj.protocol}|${obj._service_name}" => obj
  }

  load_balancer_id = hcloud_load_balancer.load_balancer[each.value._lb_name].id
  protocol         = each.value.protocol
  listen_port      = each.value.listen_port
  destination_port = each.value.destination_port
  proxyprotocol    = each.value.proxyprotocol

  dynamic "http" {
    for_each = substr(each.value.protocol, 0, 4) == "http" ? [each.value.http] : []
    content {
      sticky_sessions = http.value.sticky_sessions
      cookie_name     = http.value.cookie_name
      cookie_lifetime = http.value.cookie_lifetime
      certificates = [
        for name in http.value.certificates :
        hcloud_uploaded_certificate.uploaded_certificate[name].id
      ]
      redirect_http = http.value.redirect_http
    }
  }

  health_check {
    protocol = each.value.health_check.protocol
    interval = each.value.health_check.interval
    port     = each.value.health_check.port
    timeout  = each.value.health_check.timeout
    retries  = each.value.health_check.retries

    dynamic "http" {
      for_each = each.value.health_check.protocol == "http" ? [each.value.health_check.http] : []
      content {
        domain       = http.value.domain
        path         = http.value.path
        response     = http.value.response
        tls          = http.value.tls
        status_codes = http.value.status_codes
      }
    }
  }

  depends_on = [
    hcloud_network.network,
    hcloud_network_subnet.network_subnet,
    hcloud_load_balancer_network.load_balancer_network
  ]
}
