variable "primary_ip_spec" {
  type = map(object({
    type              = string
    datacenter        = optional(string, "hel1-dc2")
    labels            = optional(map(string), {})
    auto_delete       = optional(bool, false)
    delete_protection = optional(bool, true)
  }))
  default = {}
}

variable "server_spec" {
  type = map(object({
    image             = string
    server_type       = string
    location          = optional(string, "hel1")
    user_data         = optional(string, "")
    placement_group   = string
    primary_ip_ipv4   = string
    primary_ip_ipv6   = string
    ssh_keys          = optional(list(string), [])
    labels            = optional(map(string), {})
    delete_protection = optional(bool, false)
    networks = optional(list(object({
      name = string
      ip   = string
    })), [])
    volumes = optional(list(object({
      name      = string
      automount = bool
    })), [])
  }))
  default = {}
}

variable "firewall_spec" {
  type = map(object({
    labels   = optional(map(string), {})
    apply_to = string
    rules = list(object({
      direction   = string
      protocol    = string
      port        = string
      source_ips  = list(string)
      description = string
    }))
  }))
  default = {}
}

variable "volume_spec" {
  type = map(object({
    size              = number
    location          = optional(string, "hel1")
    format            = optional(string, "ext4")
    delete_protection = optional(bool, false)
  }))
  default = {}
}

variable "network_spec" {
  type = map(object({
    ip_range = string
    labels   = optional(map(string), {})
    subnets = list(object({
      type         = string
      network_zone = string
      ip_range     = string
    }))
    routes = optional(list(object({
      destination = string
      gateway     = string
    })), [])
  }))
  default = {}
}

variable "load_balancer_spec" {
  type = map(object({
    load_balancer_type = string
    location           = optional(string, "hel1")
    labels             = optional(map(string), {})
    network_zone       = optional(string, "")
    delete_protection  = optional(bool, false)
    algorithm = object({
      type = string
    })
    network = object({
      name = string
      ip   = string
    })
    targets = map(object({
      label_selector = string
      use_private_ip = bool
    }))
    services_http = optional(map(object({
      protocol         = string # http or https
      listen_port      = number
      destination_port = number
      proxyprotocol    = optional(bool, false)
      http = optional(object({
        sticky_sessions = optional(bool, false)
        cookie_name     = optional(string)
        cookie_lifetime = optional(number)
        certificates    = optional(list(string), [])
        redirect_http   = optional(bool, false)
      }), {})
      health_check = object({
        protocol = string
        port     = number
        interval = number
        timeout  = number
        retries  = number
        http = object({
          domain       = string
          path         = string
          response     = string
          tls          = bool
          status_codes = list(string)
        })
      })
    })), {})
    services_tcp = optional(map(object({
      listen_port      = number
      destination_port = number
      proxyprotocol    = optional(bool, false)
      health_check = object({
        protocol = string
        port     = number
        interval = number
        timeout  = number
        retries  = number
        http = object({
          domain       = string
          path         = string
          response     = string
          tls          = bool
          status_codes = list(string)
        })
      })
    })), {})
  }))
  default = {}
}

variable "floating_ips_spec" {
  type = map(object({
    type              = string
    home_location     = optional(string, "hel1")
    server_name       = optional(string, "")
    description       = optional(string)
    labels            = optional(map(string), {})
    delete_protection = optional(bool, false)
  }))
  default = {}

  validation {
    condition = alltrue([ for spec in var.floating_ips_spec : contains(["ipv4", "ipv6"], spec.type) ])
    error_message = "Floating IP type must be ipv4 or ipv6"
  }
}

variable "placement_groups" {
  type = map(object({
    type   = optional(string, "spread")
    labels = optional(map(string), {})
  }))
  default = {}
}

variable "certificates" {
  type = map(object({
    private_key = string
    certificate = string
    labels      = optional(map(string), {})
  }))
  default = {}
}

variable "ssh_keys" {
  type    = map(string)
  default = {}
}
