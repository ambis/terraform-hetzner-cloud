output "info" {
  value = <<-EOT
  Servers:
  %{~for key, server in hcloud_server.server~}
    ${server.name}: (${server.image}) (${upper(server.server_type)})
      Labels:  ${jsonencode(server.labels)}
      Addresses:
        ipv4: ${try(hcloud_primary_ip.primary_ip[var.server_spec[key].primary_ip_ipv4].ip_address, "N/A")}
        ipv6: ${try(hcloud_primary_ip.primary_ip[var.server_spec[key].primary_ip_ipv6].ip_address, "N/A")}
      Networks:
      %{~for network_spec in var.server_spec[key].networks~}
        - ${network_spec.name} (${network_spec.ip})
      %{~endfor~}
  %{~endfor~}

  EOT
}

output "data" {
  value = {
    servers : {
      for name, server_spec in hcloud_server.server : name => {
        ipv4_address = server_spec.ipv4_address
      }
    }
  }
}
