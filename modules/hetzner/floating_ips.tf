resource "hcloud_floating_ip" "floating_ip" {
  for_each = var.floating_ips_spec

  name              = each.key
  type              = each.value.type
  server_id         = length(each.value.server_name) > 0 ? hcloud_server.server[each.value.server_name].id : null
  home_location     = each.value.home_location
  description       = each.value.description
  labels            = each.value.labels
  delete_protection = each.value.delete_protection

  depends_on = [
    hcloud_server.server
  ]
}
