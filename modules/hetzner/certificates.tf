resource "hcloud_uploaded_certificate" "uploaded_certificate" {
  for_each = var.certificates

  name = each.key

  private_key = each.value.private_key
  certificate = each.value.certificate

  labels = each.value.labels

  lifecycle {
    ignore_changes = [
      private_key,
      certificate
    ]
  }
}
