resource "hcloud_firewall" "firewall" {
  for_each = var.firewall_spec

  name   = each.key
  labels = each.value.labels

  apply_to {
    label_selector = each.value.apply_to
  }

  dynamic "rule" {
    for_each = each.value.rules
    content {
      direction   = rule.value.direction
      protocol    = rule.value.protocol
      source_ips  = rule.value.source_ips
      port        = rule.value.port
      description = rule.value.description
    }
  }
}
