resource "hcloud_network" "network" {
  for_each = var.network_spec

  name     = each.key
  ip_range = each.value.ip_range
  labels   = each.value.labels
}

resource "hcloud_network_subnet" "network_subnet" {
  for_each = {
    for obj in flatten([
      for network_name, network_spec in var.network_spec : [
        for subnet_spec in network_spec.subnets :
        merge({
          _network_name = network_name
        }, subnet_spec)
      ]
    ]) : "${obj._network_name}|${obj.ip_range}" => obj
  }

  network_id   = hcloud_network.network[each.value._network_name].id
  ip_range     = each.value.ip_range
  network_zone = each.value.network_zone
  type         = each.value.type

  depends_on = [
    hcloud_server.server,
    hcloud_load_balancer.load_balancer
  ]
}

resource "hcloud_network_route" "network_route" {
  for_each = {
    for obj in flatten([
      for network_name, network_spec in var.network_spec : [
        for route_spec in network_spec.routes :
        merge({
          _network_name = network_name
        }, route_spec)
      ]
    ]) : "${obj._network_name}|${obj.destination}" => obj
  }

  network_id  = hcloud_network.network[each.value._network_name].id
  destination = each.value.destination
  gateway     = each.value.gateway

  depends_on = [
    hcloud_server.server,
    hcloud_load_balancer.load_balancer
  ]
}
