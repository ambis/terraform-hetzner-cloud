resource "hcloud_placement_group" "placement_group" {
  for_each = var.placement_groups

  name   = each.key
  type   = each.value.type
  labels = each.value.labels
}
