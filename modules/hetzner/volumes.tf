resource "hcloud_volume" "volume" {
  for_each = var.volume_spec

  name              = each.key
  size              = each.value.size
  location          = each.value.location
  format            = each.value.format
  delete_protection = each.value.delete_protection
}
