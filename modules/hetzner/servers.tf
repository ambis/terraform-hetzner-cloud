resource "hcloud_server" "server" {
  for_each = var.server_spec

  name               = each.key
  image              = each.value.image
  server_type        = each.value.server_type
  location           = each.value.location
  user_data          = each.value.user_data
  delete_protection  = each.value.delete_protection
  rebuild_protection = each.value.delete_protection
  placement_group_id = hcloud_placement_group.placement_group[each.value.placement_group].id
  ssh_keys = [
    for key in each.value.ssh_keys : hcloud_ssh_key.ssh_key[key].id
  ]
  labels = each.value.labels

  public_net {
    ipv4         = hcloud_primary_ip.primary_ip[each.value.primary_ip_ipv4].id
    ipv4_enabled = true
    ipv6         = hcloud_primary_ip.primary_ip[each.value.primary_ip_ipv6].id
    ipv6_enabled = true
  }

  lifecycle {
    ignore_changes = [
      image,
      server_type,
      location,
      user_data,
      ssh_keys,
    ]
  }
}

resource "hcloud_server_network" "server_network" {
  for_each = {
    for obj in flatten([
      for server_name, server_spec in var.server_spec : [
        for network_spec in server_spec.networks :
        merge({
          _server_name = server_name
        }, network_spec)
      ]
    ]) : "${obj._server_name}|${obj.name}" => obj
  }

  server_id  = hcloud_server.server[each.value._server_name].id
  network_id = hcloud_network.network[each.value.name].id
  ip         = each.value.ip

  depends_on = [
    hcloud_server.server,
    hcloud_network.network,
    hcloud_network_subnet.network_subnet
  ]
}

resource "hcloud_volume_attachment" "volume_attachment" {
  for_each = {
    for obj in flatten([
      for server_name, server_spec in var.server_spec : [
        for volume_spec in server_spec.volumes :
        merge({
          _server_name = server_name
        }, volume_spec)
      ]
    ]) : "${obj._server_name}|${obj.name}" => obj
  }

  server_id = hcloud_server.server[each.value._server_name].id
  volume_id = hcloud_volume.volume[each.value.name].id
  automount = each.value.automount

  depends_on = [
    hcloud_server.server,
    hcloud_volume.volume
  ]
}
