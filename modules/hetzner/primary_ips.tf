resource "hcloud_primary_ip" "primary_ip" {
  for_each = var.primary_ip_spec

  name          = each.key
  datacenter    = each.value.datacenter
  type          = each.value.type
  assignee_type = "server"
  auto_delete   = each.value.auto_delete
  labels        = each.value.labels
  delete_protection = each.value.delete_protection
}
